const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const routes = require('./routes/index');
const helpers = require('./helpers');

// Setting cors so that we can fetch data from react app
const cors = require('cors');
// create our Express app
const app = express();

// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cors());
// Exposes a bunch of methods for validating data. Used heavily on userController.validateRegister
app.use(expressValidator());

// pass variables to our templates + all requests
app.use((req, res, next) => {
  res.locals.h = helpers;
  res.locals.user = req.user || null;
  res.locals.currentPath = req.path;
  next();
});

// After all that above middleware, we finally handle our own routes!
app.use('/', routes);

module.exports = app;