const mongoose = require("mongoose");
const slug = require("slugs");
const Appliance = mongoose.model("Appliance");

exports.createAppliance = async (req, res) => {
  // console.log(req);
  // return false;
  this.slug = slug(req.body.name);
  const checkAppliances = await Appliance.findOne({
    slug:this.slug,
  });
  
  if (!checkAppliances) {
    const appliance = await new Appliance(req.body).save();
    res.send(`Successfully Created ${appliance.name}.`);
  } else {
    res.send(`An Appliance with name ${req.body.name} already exists.`);
  }
  return false;
  
};

exports.getAppliance = async (req, res) => {
  // Query the database for all Appliance
  const appliances = await Appliance.find();
  res.status(201).json({
    appliances,
  });
};

exports.editAppliance = async (req, res) => {
  const appliance = await Appliance.findOne({
    _id: req.params.id,
  });
};

exports.updateAppliance = async (req, res) => {
  //find and update the store
  //   return console.log(req.body);
  const appliance = await Appliance.findOneAndUpdate(
    {
      _id: req.params.id,
    },
    req.body,
    {
      new: true, //Return the new appliance instead of old one
      runValidators: true,
    }
  ).exec();
  res.status(201).json(appliance);
};

/**
 * This Function will be use if we need to fetch appliance using slug
 */
getApplianceBySlug = async (slug) => {
  const appliance = await Appliance.findOne({
    slug,
  });
  res.send({ appliance });
};

/**
 * This Function will be use to get specific appliance by Id
 */
exports.getApplianceById = async (req, res) => {
  const id = req.params.id;
  const query = id || {
    $exists: true,
  };
  const appliancePromise = Appliance.find({
    _id: query,
  });
  const [appliance] = await Promise.all([appliancePromise]);
  res.status(201).json({appliance});
};

/**
 * Function to delete Delete appliance by Id
 */
exports.deleteAppliance = async (req, res) => {
  await Appliance.findOneAndDelete(
    {
      _id: req.params.id,
    },
    () => {
      res.send("deleted");
    }
  );
};
