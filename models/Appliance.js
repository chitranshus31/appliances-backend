const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
const slug = require("slugs");
const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
  },
  slug: String,
  created: {
    type: Date,
    default: Date.now,
  },
  place: {
    type: String,
  },
  manufacturer: {
    type: String,
  },
  state: {
    type: String,
  },
  photo: String,
});

storeSchema.pre("save", async function (next) {
  if (!this.isModified("name")) {
    next();
    return;
  }
  this.slug = slug(this.name);
  // find the slug if it is exist in database
  const slugRegEx = new RegExp(`^(${this.slug})((-[0-9]*$)?)$`, "i");

  const storeWithSlug = await this.constructor.find({
    slug: slugRegEx,
  }); // this.constructor indicating to 'Appliance' model

  if (storeWithSlug.length) {
    this.slug = `${this.slug}-${storeWithSlug.length + 1}`;
  }
  next();
});

module.exports = mongoose.model("Appliance", storeSchema);
