const supertest = require("supertest");
const app = require("../start");
const API = "http://localhost:7777";
describe("Testing the API", () => {

  it("tests the api to fetch all the appliances", (done) => {
    supertest(API)
      .get("/api/appliances")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end(done);
  });
  
  it("tests the api to fetch specifc appliance based using id", (done) => {
    const id = '5feabaa840281bda34e64603';
    supertest(API)
      .get("/api/appliances/"+id)
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .end(done);
  });

  it("tests the post new appliance ", async () => {

    const response = await supertest(API).post('/api/create').send({
        name: 'Jest Test',
        place: 'Jest Place',
        state: 'on',
        manufacturer: 'Jest'
    });
    expect(response.status).toBe(200);
});

  afterAll((done) => {
    // Closing the DB connection
    mongoose.connection.close();
    done();
  });
});
