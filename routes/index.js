const express = require('express');
const router = express.Router();
const applianceController = require('../controllers/applianceController');

const {
    catchErrors
} = require('../handlers/errorHandlers');

// API

router.get('/api/appliances', catchErrors(applianceController.getAppliance));
router.get('/api/appliances/:id', catchErrors(applianceController.getApplianceById));
router.post('/api/update/:id', catchErrors(applianceController.updateAppliance));
router.post('/api/create', catchErrors(applianceController.createAppliance));
router.get('/api/delete/:id', catchErrors(applianceController.deleteAppliance));

module.exports = router;