## Clone this repostiory

### Your node version should be above 7 to run this project.

After clonig run the following command in your terminal:

```bash
cd appliances-backend/
npm install
```

Now to run the server

```bash
npm start
```

If you're facing error like "concurrently is not recognized" then run the following command in your terminal

```bash
npm install -g concurrently
```

If you're facing error like "nodemon is not recognized" then run the following command in your terminal

```bash
npm install -g nodemon
```
